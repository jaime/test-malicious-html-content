Release notes

* 6152b0b - (HEAD -> master) Add changelog and add to release notes (5 seconds ago) <Jaime Martinez>
* 5205566 - (HEAD -> master, tag: v1.11.0-rc5, origin/master, origin/HEAD) Use EOL and env var (8 weeks ago) <Jaime Martinez>
* aae78d4 - (tag: v1.11.0-rc4) fix cat script (8 weeks ago) <Jaime Martinez>
* d20f012 - (tag: v1.11.0-rc3) remove build needs (8 weeks ago) <Jaime Martinez>
* 6e08e88 - (tag: v1.11.0-rc2) only release (8 weeks ago) <Jaime Martinez>
* 097bdd1 - test with cat (8 weeks ago) <Jaime Martinez>
* 223ac49 - (tag: v1.11.0-rc1) remove bin (8 weeks ago) <Jaime Martinez>
